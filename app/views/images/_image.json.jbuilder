json.extract! image, :id, :description, :created_at, :updated_at, :favorite
json.url image_url(image, format: :json)
