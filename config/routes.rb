Rails.application.routes.draw do
  resources :images do
  	member do
		post 'select'
  	end

  	collection do
  		get 'selected'
  	end
  end
end
