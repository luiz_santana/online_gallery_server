json.array!(@images) do |item|
  json.extract! item, :id, :description, :picture, :favorite
  json.url image_url(item, format: :json)
end