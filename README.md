# Online Gallery

This server was made using Ruby on Rails API.
Now it has only the entity `Image` to upload files.
It's using SQLite to store database data.

The only motivation on select RoR was the simplicity on setup this basic environment.

PS: The current selected image is kept in memory. Once the server is shutdown it is removed.

## Dependencies

- Make sure you have an updated version of Ruby

To update on Mac OSX (using homebrew)

```
$ brew update
$ brew install ruby-build
$ brew install rbenv
$ rbenv install 2.4.1
$ rbenv global 2.4.1
$ rbenv rehash
```

Make sure you have imagemagick installed

### To install on Mac OSX (using homebrew)

```
$ brew install imagemagick
```

- Install ruby `Bundler`

```
$ gem install bundler
```

- Install Rails gem

```
$ gem install rails
```

- On the project folder install the dependencies

```
$ bundle install
```

- Run migrations to update database

```
$ bundle exec rake db:migrate
```


## Running server

To run the server simple run the following command:

```
$ bundle exec rails s
```